﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportProj.Interfaces;

namespace TransportProj.Router
{
    public class RouterFactory
    {
        City _city;

        public RouterFactory(City city)
        {
            _city = city;
        }

        public T CreateRouter<T>()
            where T : IRouter
        {
            return (T)Activator.CreateInstance(typeof(T), _city);
        }
    }
}
