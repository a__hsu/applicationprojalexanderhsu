﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportProj.Interfaces;

namespace TransportProj.Router
{
    /// implements "shortest" route, naive + favors straight line travel
    public sealed class DefaultRouter : AbstractRouter
    {
        public DefaultRouter(City boundary) 
            : base(boundary)
        {
        }

        public override IList<Tuple<int, int>> GetShortestPath(Tuple<int, int> start, Tuple<int, int> destination, IEnumerable<IObject> obstructions)
        {
            var path = new List<Tuple<int, int>>();
            travelPath(path, start, destination, obstructions);
            return path;
        }

        public void travelPath(IList<Tuple<int, int>> path, Tuple<int, int> start, Tuple<int, int> destination, IEnumerable<IObject> obstructions)
        {
            if (start.Equals(destination))
            {
                return;
            }
            else
            {
                Tuple<int, int> projectedPosition = destination;

                var xPosDelta = Math.Abs(destination.Item1 - start.Item1);
                var yPosDelta = Math.Abs(destination.Item2 - start.Item2);

                // try moving horizontally first
                if (xPosDelta > 0)
                {
                    var speed = Math.Min(1, xPosDelta);
                    var movement = (speed == 0 || start.Item1 < destination.Item1 ? speed : -speed);

                    var next = start.Item1 + movement;
                    if (next < 0)
                    {
                        movement = Math.Abs(start.Item1);
                    }
                    else if (next > _boundary.XMax)
                    {
                        movement = _boundary.XMax - start.Item1;
                    }

                    var nextPosition = Tuple.Create<int, int>(next, start.Item2);

                    if (!IsObstructed(nextPosition, obstructions) && movement > 0)
                    {
                        path.Add(nextPosition);
                        travelPath(path, nextPosition, destination, obstructions);
                    }
                }
                if (yPosDelta > 0)
                {
                    var speed = Math.Min(1, yPosDelta);
                    var movement = (speed == 0 || start.Item2 < destination.Item2 ? speed : -speed);

                    var next = start.Item2 + movement;
                    if (next < 0)
                    {
                        movement = Math.Abs(start.Item2);
                    }
                    else if (next > _boundary.YMax)
                    {
                        movement = _boundary.YMax - start.Item2;
                    }

                    var nextPosition = Tuple.Create<int, int>(start.Item1, start.Item2 + movement);

                    if (!IsObstructed(nextPosition, obstructions) && movement > 0)
                    {
                        path.Add(nextPosition);
                        travelPath(path, nextPosition, destination, obstructions);
                    }
                }
            }
        }
    }
}
