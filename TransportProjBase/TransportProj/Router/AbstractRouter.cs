﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportProj.Interfaces;

namespace TransportProj.Router
{
    public abstract class AbstractRouter : IRouter
    {
        protected readonly City _boundary;

        public AbstractRouter(City boundary)
        {
            _boundary = boundary;
        }


        //public abstract Tuple<int, int> GetNextPosition(Tuple<int, int> start, Tuple<int, int> destination, int maxSpeed, IEnumerable<IObject> obstructions);

        public abstract IList<Tuple<int, int>> GetShortestPath(Tuple<int, int> start, Tuple<int, int> destination, IEnumerable<IObject> obstructions);

        public virtual Tuple<int, int> GetNextPosition(Tuple<int, int> start, Tuple<int, int> destination, IList<Tuple<int, int>> moves, int maxSpeed)
        {
            var nextMove = moves.FirstOrDefault() ?? destination;

            // if speed > 1, then we might be able to move multiple times this turn
            // but only allow speedy moves along a vector
            if (moves.Count > 1 && maxSpeed > 1)
            {
                Tuple<int, int> vector = Tuple.Create<int, int>(moves[0].Item1 - start.Item1, moves[0].Item2 - start.Item2);

                var last = moves[0];
                for (var i = 1; i < Math.Min(maxSpeed, moves.Count); i++)
                {
                    var moveVector = Tuple.Create<int, int>(moves[i].Item1 - last.Item1, moves[i].Item2 - last.Item2);
                    if (!moveVector.Equals(vector))
                    {
                        break;
                    }
                    last = moves[i];
                    nextMove = moves[i];
                }
            }

            return nextMove;
        }

        public virtual bool IsObstructed(Tuple<int, int> desiredPosition, IEnumerable<IObject> obstructions)
        {
            foreach (var obs in obstructions)
            {
                if (obs.ExistsAt(desiredPosition))
                    return true;
            }
            return false;
        }
    }
}
