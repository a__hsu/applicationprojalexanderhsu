﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportProj.Interfaces;

namespace TransportProj.Router
{
    /// <summary>
    /// Cheated a bit and found an article on a table based implementation
    /// of a Dijkstra algorithm.
    /// Started down the path of creating a graph/tree but this is simpler
    /// </summary>
    public sealed class SmarterRouter : AbstractRouter
    {
        public SmarterRouter(City boundary) 
            : base(boundary)
        {
        }

        public override IList<Tuple<int, int>> GetShortestPath(Tuple<int, int> start, Tuple<int, int> destination, IEnumerable<IObject> obstructions)
        {
            var moves = new List<Tuple<int, int>>();

            // if we're already there, then don't bother
            if (start.Equals(destination))
            {
                return moves;
            }

            var distance = new Dictionary<Tuple<int, int>, int>();
            var route = new Dictionary<Tuple<int, int>, Tuple<int, int>>();

            // create a list of all allowed moves
            generateNodeLists(obstructions, start, ref distance, ref route);

            // run all nodes through the algorithm
            var nodesToProcess = distance.OrderBy(y => y.Value).Select(x => x.Key);
            runAlgorithm(nodesToProcess, distance, route);

            // run through the resulting route table to create the step-by-step move list 
            getNextMoveList(moves, start, destination, route);

            return moves;
        }


#region Private Methods
        /// <summary>
        /// Hm.. turn this into a pure function when we refactor
        /// </summary>
        /// <param name="obstructions"></param>
        private void generateNodeLists(IEnumerable<IObject> obstructions, Tuple<int, int> start, 
            ref Dictionary<Tuple<int, int>, int> distance, ref Dictionary<Tuple<int, int>, Tuple<int, int>> route)
        {
            distance = new Dictionary<Tuple<int, int>, int>();
            route = new Dictionary<Tuple<int, int>, Tuple<int, int>>();

            for (var x = 0; x < _boundary.XMax; x++)
            {
                for (var y = 0; y < _boundary.YMax; y++)
                {
                    Tuple<int, int> location = Tuple.Create<int, int>(x, y);
                    if (!IsObstructed(location, obstructions))
                    {
                        if (location.Equals(start))
                        {
                            distance.Add(location, 0);
                        }
                        else
                        {
                            distance.Add(location, int.MaxValue);
                        }

                        route.Add(location, Tuple.Create<int, int>(-1, -1));
                    }
                }
            }
        }

        private void getNextMoveList(IList<Tuple<int, int>> list, Tuple<int, int> start, Tuple<int, int> location,
            Dictionary<Tuple<int, int>, Tuple<int, int>> route)
        {
            var nextLocation = route[location];
            if (!nextLocation.Equals(start))
            {
                getNextMoveList(list, start, nextLocation, route);     // add reversed in order to simplify calculating true "next"
                list.Add(nextLocation);
            }
        }

        /// <summary>
        /// Recursive implementation of shortest pathing algorithm
        /// For each node from start, check all possible neighbors and add a "cost" 
        /// associated with the move - in this case, modeled as a grid so each move = +1
        /// If we've already seen the node, determine if it's cheaper to get to this node from current
        /// </summary>
        /// <param name="nodesToProcess"></param>
        private void runAlgorithm(IEnumerable<Tuple<int, int>> nodesToProcess,
            Dictionary<Tuple<int, int>, int> distance, Dictionary<Tuple<int, int>, Tuple<int, int>> route)
        {
            if (nodesToProcess.Count() > 0)
            {
                var node = nodesToProcess.First();

                var possibleMoves = new List<Tuple<int, int>>
                {
                    Tuple.Create<int, int>(node.Item1 + 1, node.Item2),
                    Tuple.Create<int, int>(node.Item1 - 1, node.Item2),
                    Tuple.Create<int, int>(node.Item1, node.Item2 + 1),
                    Tuple.Create<int, int>(node.Item1, node.Item2 - 1),
                };

                foreach (var move in possibleMoves)
                {
                    if (distance.ContainsKey(move))
                    {
                        if (distance[node] + 1 < distance[move])
                        {
                            distance[move] = distance[node] + 1;
                            route[move] = node;
                        }
                    }
                }

                runAlgorithm(nodesToProcess.Skip(1), distance, route);
            }
        }
#endregion Private Methods
    }
}
