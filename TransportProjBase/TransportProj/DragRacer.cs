﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj
{
    public class DragRacer : Car
    {
        public DragRacer(string id, int x, int y, City city) : base(id, x, y, city)
        {
        }

        public DragRacer(string id, int xPos, int yPos, City city, Passenger passenger) : base(id, xPos, yPos, city, passenger)
        {

        }

        public override int MaxSpeed
        {
            get
            {
                return 3;
            }
        }

        protected override void WritePositionToConsole()
        {
            Console.WriteLine($"Drag Racer {this.Id} moved to x - {this.XPos} y - {this.YPos}");
        }
    }
}
