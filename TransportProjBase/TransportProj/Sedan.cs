﻿using System;

namespace TransportProj
{
    public class Sedan : Car
    {
        public Sedan(string id, int x, int y, City city) : base(id, x, y, city)
        {
        }

        public Sedan(string id, int xPos, int yPos, City city, Passenger passenger) : base(id, xPos, yPos, city, passenger)
        {
        }

        protected override void WritePositionToConsole()
        {
            Console.WriteLine($"Sedan {this.Id} moved to x - {this.XPos} y - {this.YPos}");
        }
    }
}
