﻿
using System;
using System.Collections.Generic;
using TransportProj.Interfaces;

namespace TransportProj
{
    public class City : IObject
    {
        public int YMax { get; private set; }
        public int XMax { get; private set; }

        public City(int xMax, int yMax)
        {
            XMax = xMax;
            YMax = yMax;
            Obstacles = new List<IObject>();
        }

        public Car AddCarToCity(int xPos, int yPos)
        {
            Sedan car = new Sedan("Car1", xPos, yPos, this, null);

            return car;
        }

        public Passenger AddPassengerToCity(int startXPos, int startYPos, int destXPos, int destYPos)
        {
            Passenger passenger = new Passenger("P1", startXPos, startYPos, destXPos, destYPos, this);

            return passenger;
        }

        public IList<IObject> Obstacles { get; set; }

        public Tuple<int, int> Corner
        {
            get
            {
                return Tuple.Create<int, int>(0, 0);
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public Tuple<int, int> Size
        {
            get
            {
                return Tuple.Create<int, int>(XMax, YMax);
            }
        }

        public void AddObstacle(IObject obstacle)
        {
            Obstacles.Add(obstacle);
        }

        public bool ExistsAt(Tuple<int, int> location)
        {
            throw new NotImplementedException();
        }
    }
}
