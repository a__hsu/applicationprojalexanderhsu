﻿using System;
using System.Threading;
using System.Linq;
using TransportProj.Interfaces;
using TransportProj.Router;
using System.Threading.Tasks;
using System.Collections.Generic;
using TransportProj.Visualization;
using TransportProj.MatchMaker;

namespace TransportProj
{
    class Program
    {
        private static Dictionary<Type, Type> _drawingStrategies;

        private static City _city;
        private static int _cityLength = 13;
        private static int _cityWidth = 10;

        private static IList<IVehicle> _cars;
        private static IList<Passenger> _passengers;

        private static CarFactory _carFactory;
        private static RouterFactory _routerFactory;

        private static IMatchMakerStrategy _matchMakerStrategy;

        static void Main(string[] args)
        {
            Bootstrap();

            foreach(var car in _cars)
            {
                Console.WriteLine($"{car.Id} at ({car.XPos},{car.YPos}) - HasPassenger:{car.Passenger != null}");
            }
            foreach(var passenger in _passengers)
            {
                Console.WriteLine($"{passenger.Id} at ({passenger.StartingXPos},{passenger.StartingYPos}) > ({passenger.DestinationXPos},{passenger.DestinationYPos})");
            }

            var objects = CollectDrawableObjects(_city, _cars.Cast<IObject>().ToArray(), _passengers);
            DrawBoard(objects, _drawingStrategies);

            // a bit of a race condition here since passenger position is completely dependent on coupling with Car
            // we would like the passenger to disembark at destination (get out of car) within the same Tick loop
            // so we've got to add an extra "tick"

            // also naive in terms of Car disposition - I'd assume that the Car should move on but we're just going to assume that it sits parked at passenger destination
            while (_passengers.Count(x => !x.IsAtDestination()) > 0)
            {
                Tick(_cars, _passengers, _city);
            }
            Tick(_cars, _passengers, _city);

            return;
        }

        /// <summary>
        /// Orchestration engine for handling multiple passengers and cars
        /// </summary>
        /// <param name="cars"></param>
        /// <param name="passengers"></param>
        /// <param name="city"></param>
        private static void Tick(IList<IVehicle> cars, IList<Passenger> passengers, City city)
        {
            var objects = CollectDrawableObjects(city, cars.Cast<IObject>().ToArray(), passengers);

            // we need to "tick" for each passenger looking for a ride - if they're all at their destinations, then no work!
            var passengersNeedingRides = passengers.Where(x => !x.IsAtDestination());
            foreach (var passenger in passengersNeedingRides)
            {
                Tick(cars, passenger, city);
            }
            
            if (passengersNeedingRides.Count() == 0 && passengers.Count > 0)
                Tick(cars, passengers.First(), city);

            // clean up needy passenger list - if you're not at your destination or in a car, then continue to report
            var passengersStillNeedingRides = passengers.Where(x => !x.IsAtDestination() || x.Car != null).ToArray();
            passengers.Clear();
            foreach (var passenger in passengersStillNeedingRides)
            {
                passengers.Add(passenger);
            }

            DrawBoard(objects, _drawingStrategies);
        }

        /// <summary>
        /// Takes one action (move the car one spot or pick up the passenger).
        /// </summary>
        /// <param name="cars">The car to move</param>
        /// <param name="passenger">The passenger to pick up</param>
        private static void Tick(IList<IVehicle> cars, Passenger passenger, City city)
        {
            var destination = Tuple.Create<int, int>(passenger.StartingXPos, passenger.StartingYPos);

            var car = _matchMakerStrategy.SelectVehicle(_routerFactory.CreateRouter<SmarterRouter>(), cars, passenger, city);

            if (car == null)
                return;

            if (car.Passenger != null)
            {
                destination = Tuple.Create<int, int>(car.Passenger.DestinationXPos, car.Passenger.DestinationYPos);
            }

            if (car is ISupportsReservation && ((ISupportsReservation)car).ReservedBy == null)
            {
                ((ISupportsReservation)car).AddReservation(passenger);
            }

            // if we're at destination - decide what the passenger needs to do
            if (Tuple.Create<int, int>(car.XPos, car.YPos).Equals(destination))
            {
                if (car.Passenger == null && passenger == null)
                {
                    return;
                }

                if (car.Passenger == null && passenger != null)
                {
                    passenger.GetInCar(car); // only 1 action allowed per "tick"
                    //DrawBoard(objects, _drawingStrategies);
                    return;
                }
                else if (car.Passenger != null)
                {
                    passenger.GetOutOfCar();  // clean up the passenger "list"
                    Console.WriteLine($"{passenger.Id} at ({passenger.GetCurrentXPos()},{passenger.GetCurrentYPos()}) > waves goodbye!");
                    //DrawBoard(objects, _drawingStrategies);
                    return;
                }
            }

            var router = _routerFactory.CreateRouter<SmarterRouter>();
            var path = router.GetShortestPath(car.GetPosition(), destination, _city.Obstacles);
            Tuple<int, int> nextPosition = router.GetNextPosition(car.GetPosition(), destination, path, car.MaxSpeed);
            Console.WriteLine($">> Next s/b ({nextPosition.Item1},{nextPosition.Item2})");

            // Let's temporarily let "tick" is the driver/master orchestrator so let's let "Tick" decide how to move the car to the next Position
            if (nextPosition.Item1 > car.GetPosition().Item1)
            {
                car.MoveRight(Math.Abs(nextPosition.Item1 - car.GetPosition().Item1));
            }
            else if (nextPosition.Item1 < car.GetPosition().Item1)
            {
                car.MoveLeft(Math.Abs(nextPosition.Item1 - car.GetPosition().Item1));
            }
            else if (nextPosition.Item2 > car.GetPosition().Item2)
            {
                car.MoveUp(Math.Abs(nextPosition.Item2 - car.GetPosition().Item2));
            }
            else if (nextPosition.Item2 < car.GetPosition().Item2)
            {
                car.MoveDown(Math.Abs(nextPosition.Item2 - car.GetPosition().Item2));
            }

            Console.WriteLine($"{car.Id} at ({car.XPos},{car.YPos}) - HasPassenger:{car.Passenger != null}");
            //DrawBoard(objects, _drawingStrategies);
        }


        #region Drawing system
        private static IList<IObject> CollectDrawableObjects(City city, IList<IObject> cars, IList<Passenger> passengers)
        {
            var objects = new List<IObject>
            {
                city,
            };
            objects.AddRange(city.Obstacles);
            objects.AddRange(cars);
            objects.AddRange(passengers);
            return objects;
        }

        private static void DrawBoard(IList<IObject> objects, Dictionary<Type, Type> drawingStrategies)
        {
            Canvas<string> canvas = new Canvas<string>(_city.XMax, _city.YMax);
            foreach (var obj in objects)
            {
                var type = drawingStrategies[obj.GetType()];
                var drawer = Activator.CreateInstance(type);
                var method = type.GetMethod("Draw");
                method.Invoke(drawer, new object[] { obj, canvas });
            }
            Console.Write(canvas.GetOutput());
        }
        #endregion Drawing system


        private static void Bootstrap()
        {
            Random rand = new Random();
            int CityLength = _cityLength;
            int CityWidth = _cityWidth;

            _city = new City(CityLength, CityWidth);
            _city.AddObstacle(new Building(Tuple.Create<int, int>(4, 4), Tuple.Create<int, int>(4, 5)));
            _city.AddObstacle(new Building(Tuple.Create<int, int>(2, 2), Tuple.Create<int, int>(2, 2)));

            _routerFactory = new RouterFactory(_city);
            _carFactory = new CarFactory(_city);

            _passengers = new List<Passenger>();

            Tuple<int, int> passengerStart = generateValidRandomLocation(rand, _city.Obstacles);
            Tuple<int, int> passengerDestination = generateValidRandomLocation(rand, _city.Obstacles);
            _passengers.Add(new Passenger("PA", passengerStart.Item1, passengerStart.Item2, passengerDestination.Item1, passengerDestination.Item2, _city));

            passengerStart = generateValidRandomLocation(rand, _city.Obstacles);
            passengerDestination = generateValidRandomLocation(rand, _city.Obstacles);
            _passengers.Add(new Passenger("PB", passengerStart.Item1, passengerStart.Item2, passengerDestination.Item1, passengerDestination.Item2, _city));

            _cars = new List<IVehicle>();

            Tuple<int, int> carStart = generateValidRandomLocation(rand, _city.Obstacles);
            _cars.Add(_carFactory.CreateCar<DragRacer>("Car1", carStart.Item1, carStart.Item2));

            // add another car - make sure the new car doesn't take same location as previous car(s)
            carStart = generateValidRandomLocation(rand, _city.Obstacles.Union(_cars.Cast<IObject>()));
            _cars.Add(_carFactory.CreateCar<Racer>("Car2", carStart.Item1, carStart.Item2));

            _drawingStrategies = new Dictionary<Type, Type>();
            _drawingStrategies.Add(typeof(City), typeof(DrawCity));
            _drawingStrategies.Add(typeof(Building), typeof(DrawBuilding));
            _drawingStrategies.Add(typeof(Racer), typeof(DrawCar));
            _drawingStrategies.Add(typeof(Sedan), typeof(DrawCar));
            _drawingStrategies.Add(typeof(DragRacer), typeof(DrawCar));
            _drawingStrategies.Add(typeof(Passenger), typeof(DrawPassenger));

            _matchMakerStrategy = new FastestToPassengerMatchMaker();
        }


        /// <summary>
        /// once we start adding buildings and other obstacles (like other cars) into the mix, then we need to constrain 
        /// object placement to "valid" locations - hence this method.
        /// </summary>
        /// <param name="rand"></param>
        /// <param name="obstacles"></param>
        /// <returns></returns>
        private static Tuple<int, int> generateValidRandomLocation(Random rand, IEnumerable<IObject> obstacles)
        {
            Tuple<int, int> location;
            do
            {
                location = Tuple.Create<int, int>(rand.Next(_cityLength - 1), rand.Next(_cityWidth - 1));
            } while (IsBlocked(location, obstacles));

            return location;
        }

        /// <summary>
        /// Tests the location against the list of obstacles
        /// </summary>
        /// <param name="location"></param>
        /// <param name="obstacles"></param>
        /// <returns></returns>
        private static bool IsBlocked(Tuple<int, int> location, IEnumerable<IObject> obstacles)
        {
            foreach(var obstacle in obstacles)
            {
                if (obstacle.ExistsAt(location))
                    return true;
            }
            return false;
        }
    }
}
