﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using TransportProj.Interfaces;

namespace TransportProj
{
    public class Passenger : IObject
    {
        private bool _isAtDestination = false;

        public string Id { get; private set; }

        public int StartingXPos { get; private set; }
        public int StartingYPos { get; private set; }
        public int DestinationXPos { get; private set; }
        public int DestinationYPos { get; private set; }
        public IVehicle Car { get; set; }
        public City City { get; private set; }

        public Tuple<int, int> Corner
        {
            get
            {
                return Tuple.Create<int, int>(GetCurrentXPos(), GetCurrentYPos());
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public Tuple<int, int> Size
        {
            get
            {
                return Tuple.Create<int, int>(1, 1);
            }
        }

        public Passenger(string id, int startXPos, int startYPos, int destXPos, int destYPos, City city)
        {
            Id = id;
            StartingXPos = startXPos;
            StartingYPos = startYPos;
            DestinationXPos = destXPos;
            DestinationYPos = destYPos;
            City = city;
        }

        public void GetInCar(IVehicle car)
        {
            Car = car;
            car.PickupPassenger(this);
            Console.WriteLine($"Passenger {this.Id} got in car {Car.Id}.");
        }

        // adding drop off (car's passenger goes to null) since the pattern appears to be passenger-centric.
        public void GetOutOfCar()
        {
            _isAtDestination = true;
            Car.DropoffPassenger();
            Console.WriteLine($"Passenger {this.Id} got out of car {Car.Id}.");
            Car = null;
        }

        public int GetCurrentXPos()
        {
            if(Car == null)
            {
                return _isAtDestination ? DestinationXPos : StartingXPos;
            }
            else
            {
                return Car.XPos;
            }
        }

        public int GetCurrentYPos()
        {
            if (Car == null)
            {
                return _isAtDestination ? DestinationYPos : StartingYPos;
            }
            else
            {
                return Car.YPos;
            }
        }

        public bool IsAtDestination()
        {
            if (!_isAtDestination)
                _isAtDestination = GetCurrentXPos() == DestinationXPos && GetCurrentYPos() == DestinationYPos;
            return _isAtDestination;
        }

        // this is a little weird but the Passenger is the one deciding to browse to a site on position 
        // updates so we'll dump the site and method into the Passenger object.
        // this should probably be injected.
#pragma warning disable CS1998 // This async method lacks 'await' operators and will run synchronously. Consider using the 'await' operator to await non-blocking API calls, or 'await Task.Run(...)' to do CPU-bound work on a background thread.
        public async void MoveAction(int x, int y)
#pragma warning restore CS1998 // This async method lacks 'await' operators and will run synchronously. Consider using the 'await' operator to await non-blocking API calls, or 'await Task.Run(...)' to do CPU-bound work on a background thread.
        {
            Console.WriteLine("Browse to veyo.com");

            //HttpClient client = new HttpClient();
            //var result = await client.GetAsync("http://veyo.com").ConfigureAwait(false);
            //var content = await result.Content.ReadAsStringAsync().ConfigureAwait(false);

            //Console.WriteLine("Veyo.com: " + content.Substring(0, 3));
        }

        public bool ExistsAt(Tuple<int, int> location)
        {
            throw new NotImplementedException();
        }
    }
}
