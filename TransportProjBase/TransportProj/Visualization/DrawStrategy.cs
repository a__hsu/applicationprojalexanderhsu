﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportProj.Interfaces;

namespace TransportProj.Visualization
{
    public class DrawStrategy<T, S> 
        where T : IObject
    {
        protected Func<S> getPoint;

        public virtual void Draw(T obj, Canvas<S> canvas)
        {
            for (var i = 0; i < obj.Size.Item1; i++)
            {
                for (var j = 0; j < obj.Size.Item2; j++)
                {
                    canvas.SetAt(getPoint(), obj.Corner.Item1 + i, obj.Corner.Item2 + j);
                }
            }
        }
    }
}
