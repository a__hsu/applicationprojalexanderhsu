﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj.Visualization
{
    class DrawBuilding : DrawStrategy<Building, string>
    {
        public DrawBuilding()
            : base()
        {
            this.getPoint = pointCharacter;
        }

        protected string pointCharacter()
        {
            return "X";
        }

        public override void Draw(Building obj, Canvas<string> canvas)
        {
            base.Draw(obj, canvas);
        }
    }
}
