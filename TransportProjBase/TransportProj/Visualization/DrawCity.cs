﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj.Visualization
{
    public class DrawCity : DrawStrategy<City, string>
    {
        public DrawCity()
            : base()
        {
            this.getPoint = pointCharacter;
        }

        protected string pointCharacter()
        {
            return "-";
        }
    }
}
