﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj.Visualization
{
    public class DrawCar : DrawStrategy<Car, string>
    {
        public DrawCar()
            : base()
        {
            this.getPoint = pointCharacter;
        }

        protected string pointCharacter()
        {
            return "C";
        }

        public override void Draw(Car obj, Canvas<string> canvas)
        {
            this.getPoint = (() => {
                var c = string.IsNullOrEmpty(obj.Id) ? 'C' : obj.Id.Last();
                if (obj.Passenger != null)
                    c -= (char)15;
                return c.ToString();
            });

            base.Draw(obj, canvas);
        }
    }
}
