﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj.Visualization
{
    public class Canvas<T> 
    {
        private T[,] _canvas;

        public Canvas(int x, int y)
        {
            _canvas = (T[,])Array.CreateInstance(typeof(T), x, y);
        }

        public void SetAt(T value, int x, int y)
        {
            if (_canvas.GetLength(0) > x && _canvas.GetLength(1) > y)
            {
                _canvas[x, y] = value;
            }
        }

        public T GetOutput()
        {
            if (typeof(T) == typeof(string))
            {
                StringBuilder sb = new StringBuilder();
                for (var y = _canvas.GetLength(1) - 1; y >= 0; y--)
                {
                    sb.Append(y.ToString().Last());
                    for (var x = 0; x < _canvas.GetLength(0); x++)
                    {
                        sb.Append(_canvas[x, y]);
                    }
                    sb.Append("\n");
                }
                sb.Append(" ");
                for (var x = 0; x < _canvas.GetLength(0); x++)
                {
                    sb.Append(x.ToString().Last());
                }
                sb.Append("\n");
                return (T)Convert.ChangeType(sb.ToString(), typeof(T));
            }
            return default(T);
        }
    }
}
