﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj.Visualization
{
    public class DrawPassenger : DrawStrategy<Passenger, string>
    {
        public DrawPassenger()
            : base()
        {
            this.getPoint = pointCharacter;
        }

        protected string pointCharacter()
        {
            return "P";
        }

        public override void Draw(Passenger obj, Canvas<string> canvas)
        {
            if (obj.Car != null)
            {
                this.getPoint = (() => {
                    var c = string.IsNullOrEmpty(obj.Id) ? 'd' : obj.Id.ToLowerInvariant().Last();
                    return c.ToString();
                });
            }
            else
            {
                this.getPoint = (() => {
                    return string.IsNullOrEmpty(obj.Id) ? "C" : obj.Id.Last().ToString();
                });
            }
            
            base.Draw(obj, canvas);
        }
    }
}
