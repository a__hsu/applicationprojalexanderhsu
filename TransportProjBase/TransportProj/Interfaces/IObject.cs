﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj.Interfaces
{
    public interface IObject
    {
        Tuple<int, int> Corner { get; set; }
        
        Tuple<int, int> Size { get; }

        bool ExistsAt(Tuple<int, int> location);
    }
}
