﻿using System;
using System.Collections.Generic;
using TransportProj.Interfaces;

namespace TransportProj.Interfaces
{
    public interface IRouter
    {
        IList<Tuple<int, int>> GetShortestPath(Tuple<int, int> start, Tuple<int, int> destination, IEnumerable<IObject> obstructions);
        Tuple<int, int> GetNextPosition(Tuple<int, int> start, Tuple<int, int> destination, IList<Tuple<int, int>> moves, int maxSpeed);
    }
}