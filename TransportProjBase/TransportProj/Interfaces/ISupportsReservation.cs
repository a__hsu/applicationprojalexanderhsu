﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj.Interfaces
{
    public interface ISupportsReservation
    {
        void AddReservation(Passenger passenger);
        Passenger ReservedBy { get; }
    }
}
