﻿using System;

namespace TransportProj.Interfaces
{
    public interface IVehicle
    {
        City City { get; }
        string Id { get; }
        int MaxSpeed { get; }
        Passenger Passenger { get; }
        int XPos { get; }
        int YPos { get; }

        void DropoffPassenger();
        Tuple<int, int> GetPosition();
        void MoveAction(int x, int y);
        void MoveDown(int speed);
        void MoveLeft(int speed);
        void MoveRight(int speed);
        void MoveUp(int speed);
        void PickupPassenger(Passenger passenger);
    }
}