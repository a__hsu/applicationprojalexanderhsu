﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj.Interfaces
{
    public interface IMatchMakerStrategy
    {
        IVehicle SelectVehicle(IRouter router, IList<IVehicle> vehicles, Passenger passenger, City city);
    }
}
