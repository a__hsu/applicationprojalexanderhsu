﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportProj.Interfaces;

namespace TransportProj.MatchMaker
{
    public class FastestToPassengerMatchMaker : IMatchMakerStrategy
    {
        public IVehicle SelectVehicle(IRouter router, IList<IVehicle> vehicles, Passenger passenger, City city)
        {
            // search for the car that has reserved or picked up this passenger
            IVehicle car = vehicles.Where(c => c is ISupportsReservation && ((ISupportsReservation)c).ReservedBy?.Id == passenger.Id).FirstOrDefault();
            if (car == null)
            {
                Dictionary<string, int> moves = new Dictionary<string, int>();
                Parallel.ForEach(vehicles.Where(x => x is ISupportsReservation && ((ISupportsReservation)x).ReservedBy == null), (c) =>
                {
                    // divide by max speed in order to figure out which car would get to the passenger the fastest
                    // integer math so just add 1 if it doesn't divide out evenly
                    moves.Add(c.Id, router.GetShortestPath(c.GetPosition(), passenger.Corner, city.Obstacles).Count / c.MaxSpeed 
                        + (router.GetShortestPath(c.GetPosition(), passenger.Corner, city.Obstacles).Count % c.MaxSpeed > 0 ? 1 : 0));
                });
                var shortestPathToPassenger = moves.Values.Min();
                car = vehicles.Where(c => c.Id == moves.Where(x => x.Value == shortestPathToPassenger).FirstOrDefault().Key)?.FirstOrDefault();
            }

            if (car == null)
            {
                // if we haven't picked a car yet, then pick the first empty car - or, if there are no available cars, then 
                // the passenger is out of luck - wait for the next tick
                car = vehicles.Where(c => c is ISupportsReservation && ((ISupportsReservation)c).ReservedBy == null).FirstOrDefault();
            }

            return car;
        }
    }
}
