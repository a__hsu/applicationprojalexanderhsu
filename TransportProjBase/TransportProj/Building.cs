﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportProj.Interfaces;

namespace TransportProj
{
    public class Building : IObject
    {
        public Building(Tuple<int, int> corner, Tuple<int, int> size)
        {
            Corner = corner;
            Size = size;
        }

        public Tuple<int, int> Corner { get; set; }

        public Tuple<int, int> Size { get; }

        public bool ExistsAt(Tuple<int, int> location)
        {
            if (location.Item1 >= Corner.Item1 && location.Item1 < Corner.Item1 + Size.Item1
                && location.Item2 >= Corner.Item2 && location.Item2 < Corner.Item2 + Size.Item2)
            {
                return true;
            }
            return false;
        }
    }
}
