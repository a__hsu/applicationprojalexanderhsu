﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj
{
    public class CarFactory
    {
        City _city;

        public CarFactory(City city)
        {
            _city = city;
        }

        public T CreateCar<T>(string id, int xPos, int yPos)
            where T: Car
        {
            return (T)Activator.CreateInstance(typeof(T), id, xPos, yPos, _city);
        }
    }
}
