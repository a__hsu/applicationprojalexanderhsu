﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj
{
    class Racer : Car
    {
        public Racer(string id, int x, int y, City city) : base(id, x, y, city)
        {
        }

        public Racer(string id, int xPos, int yPos, City city, Passenger passenger) : base(id, xPos, yPos, city, passenger)
        {

        }

        public override int MaxSpeed
        {
            get
            {
                return 2;
            }
        }

        protected override void WritePositionToConsole()
        {
            Console.WriteLine($"Racer {this.Id} moved to x - {this.XPos} y - {this.YPos}");
        }
    }
}
