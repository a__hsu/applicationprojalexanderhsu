﻿using System;
using TransportProj.Interfaces;

namespace TransportProj
{
    public abstract class Car : IObject, ISupportsReservation, IVehicle
    {
        Action<int, int> moveAction;

        public string Id { get; private set; }

        public int XPos { get; protected set; }
        public int YPos { get; protected set; }
        public Passenger Passenger { get; private set; }
        public City City { get; private set; }

        public Car(string id, int x, int y, City city) 
            : this(id, x, y, city, null)
        {

        }

        public Car(string id, int xPos, int yPos, City city, Passenger passenger)
        {
            Id = id;
            XPos = xPos;
            YPos = yPos;
            City = city;
            Passenger = passenger;
            moveAction = MoveAction;
        }

        public virtual int MaxSpeed
        {
            get
            {
                return 1;
            }
        }

        public virtual Tuple<int, int> Corner
        {
            get
            {
                return GetPosition();
            }
            set
            {
                XPos = value.Item1;
                YPos = value.Item2;
            }
        }
        public virtual Tuple<int, int> Size
        {
            get
            {
                return Tuple.Create<int, int>(1, 1);
            }
        }

        protected virtual void UpdatePosition()
        {
            moveAction(XPos, YPos);
            WritePositionToConsole();
        }

        protected virtual void WritePositionToConsole()
        {
            Console.WriteLine($"Car {this.Id} moved to x - {this.XPos} y - {this.YPos}");
        }

        public void PickupPassenger(Passenger passenger)
        {
            Passenger = passenger;
            _reservation = passenger;
            moveAction = Passenger.MoveAction;
        }
        public void DropoffPassenger()
        {
            Passenger = null;
            _reservation = null;
            moveAction = MoveAction;
        }

        public virtual void MoveUp(int speed)
        {
            speed = ClampSpeed(speed);

            if (YPos < City.YMax + speed)
            {
                YPos += speed;
                UpdatePosition();
            }
        }

        public virtual void MoveDown(int speed)
        {
            speed = ClampSpeed(speed);

            if (YPos - speed >= 0)
            {
                YPos -= speed;
                UpdatePosition();
            }
        }

        public virtual void MoveRight(int speed)
        {
            speed = ClampSpeed(speed);

            if (XPos < City.XMax + speed)
            {
                XPos += speed;
                UpdatePosition();
            }
        }

        public virtual void MoveLeft(int speed)
        {
            speed = ClampSpeed(speed);

            if (XPos - speed >= 0)
            {
                XPos -= speed;
                UpdatePosition();
            }
        }

        protected int ClampSpeed(int speed)
        {
            if (speed > MaxSpeed)
                return MaxSpeed;
            return speed;
        }

        public Tuple<int, int> GetPosition()
        {
            return Tuple.Create<int, int>(XPos, YPos);
        }

        public virtual bool ExistsAt(Tuple<int, int> location)
        {
            if (location.Equals(this.GetPosition()))
            {
                return true;
            }
            return false;
        }


        public void MoveAction(int x, int y)
        {
            
        }

        private Passenger _reservation;

        public void AddReservation(Passenger passenger)
        {
            if (_reservation == null)
                _reservation = passenger;
        }
        public Passenger ReservedBy
        {
            get
            {
                return _reservation;
            }
        }
    }
}
